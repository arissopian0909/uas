#include <iostream>
#include "tampilNilai.h"
#include "hurufMutu.h"

using namespace std;

int main(){
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;

    tampilNilai(quiz, absen, uts, uas, tugas);
    hurufMutu(Huruf_Mutu, quiz, absen, uts, uas, tugas);
    nilaiAkhir(Huruf_Mutu);

    return 0;
}
